# Cucumber and Selenide tutorial
This project demonstrates how to test a web application using Cucumber, Selenide and Docker.

# Preparations
In order to use this project you'll need to download Docker for the platform you use:

* Docker for Mac

* Docker for Windows

You'll also need to install JDK 8, maven and an IDE like Eclipse or IntelliJ.

# The website under test
The example project is a very simple website that will let the user enter a string and get the reversed string returned. The site will be run on nginx inside a docker container. You can review the Dockerfile to see how the image is populated with data from the project. 
You can examine the code for the site in the directory "StringReverserWebsite".

# The tests
The tests are located in the directory "src/test". 

The test cases are specified using Gherkin and are located in the file "src/test/resources/stringreverser.feature".

Gherkin is domain specific language for describing behaviours. Behaviours are described in feature files that consists of one or more scenarios. Each scenario describes a behaviour and the expected outcome by using the format:

```gherkin
Scenario: Some specific behaviour
Given some precondition
When the user does some action
Then the outcome should be
```

The Gherkin descriptions are implemented as different cucumber steps that will be defining code that should be executed for each part of the scenario. The cucumber steps are defined in the file "src/test/java/bindings/Stringreverser.java".

Read more about [Gherkin and Cucumber](https://cucumber.io).


To drive the tests, a page object is used that represents the page we want to test and contains the code that automates interactions with the page. Page objects is a pattern to seperate the automation of the browsers from the test code.

# Selenide
Selenide is a framework for automated testing of web applications. It is built on top of the Selenium web driver. Selenide makes some of the more tedious and complex things in Selenium much simpler by:

* Transparent handling of the web driver in the background reducing the need for boilerplate code

* Smart waiting that makes testing AJAX-based applications much simpler

* Convenience methods that makes it simpler to find and interact with elements on the web page

* Automatic screen shot when a test fails

Read more about [Selenide](https://selenide.org)

# The docker-compose file
The docker-compose.yml file describes the infrastructure for setting up the Nginx webserver, the Selenium grid hub and a Selenium node 
running Chrome. The Chrome node has a VNC server so if you want to see the tests running in 
the browser you just connect to it with a VNC client (password is "secret").

* Chrome node VNC port is 5910

# Start the environment
To start all the necessary containers just navigate to the folder containing the docker-compose.yml file from a terminal (or 
PowerShell if your'e using Windows). Then enter the command

__docker-compose build__

This will build the Nginx webserver image and populate it with data. If you change any data or remove the images you will need to run this command again.

Now you're ready to start the containers by entering the command:

__docker-compose up -d__

This will start the containers in daemon (background) mode. First time you run the docker-compose file it will download images for Selenium Grid Hub, Selenium Node Chrome and Nginx from Docker Hub. The images will be stored locally on your machine so next time you start run the docker-compose file it will 
be much faster. You can also remove the images afterwards if you which by using the command:

__docker rmi <image_id>__

# Browse to the site from your local machine
Begin by opening the site in any browser on your local machine by navigating to 'http://localhost:8010'.

# Verify that Selenium Grid is up and running
To verify that Selenium Grid is up and running navigate to 'http://localhost:4444/grid/console'. The page should show that there is a Chrome node running.

# Running the tests from an IDE
To run the tests just open your preferred IDE and run the tests via maven. If you connect to the nodes through VNC you will be able to see the tests running in the browsers.

# Running the tests from a terminal or command line
If you prefer you can run the tests from a terminal or command line as well. Open a terminal or command line and go navigate to the SeleniumTests folder and enter the command:
__mvn test__

# Stopping the environment
To stop the environment when the tests are done, enter the command:

__docker-compose down__

# Discussion
## Docker
A benefit of using Docker Containers for setting up a Selenium Grid is that anyone can run the tests without having to install the Selenium Server, web drivers, different browsers etc. As long as Docker is installed you are good to go.
After the test have been run all the containers are disposed of and the only thing remaining on your computer is the docker images for Selenium and nginx. These can be removed with the command __docker rmi <imageid>__ to save disk space.

Since the Selenium Grid and the Selenium node are based on Linux images it will limit the browsers that can be used. There are docker images for Chrome and Firefox Selenium nodes but there are no images for Microsoft Edge, Internet Explorer or
Safari since these require different operating systems to run. You can however set up these nodes on other machines and connect to them form Selenium hub running inside docker but it will make the setup more fragile since it depends on external resources.

## Gherkin and Cucumber
Gherkin allows us to describe the behaviour of the system in plain english by stating different examples (known as specification by example). These scenarios are easier to communicate and discuss than a more traditional requirements specification.

Cucumber makes it easy to create automated steps for the scenarios described by Gherkin. It makes it easier to automatically test the features using the business acceptance critereas.

## Selenide
Selenide removes much of the complexity that is present in Selenium. We don't need to keep track of the web driver reference and don't need to write boilerplate code to handle it. It also provides nice convenience methods for finding and interacting with elements on the web page.

Selenide combines browser automation and tests in the same framework. While this provides a powerful and convenient way to test a web application, there is a risk that browser automation code and test code becomes mixed up. Try to follow the page object pattern to clearly separate the browser interaction from the tests.