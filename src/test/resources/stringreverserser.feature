@run
Feature: stringreverser

  Scenario: reverse string
    Given the user is on the stringreverser page
    When the user enters the string "abc"
    Then the reversed string should be "cba"

  Scenario: identify palindrome
    Given the user is on the stringreverser page
    When the user enters the string "racecar"
    Then the reversed string should be "racecar"
    And be identified as a palindrome
