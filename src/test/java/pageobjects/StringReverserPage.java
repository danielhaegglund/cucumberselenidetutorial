package pageobjects;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.title;

public class StringReverserPage {

    public String getPageTitle()
    {
        return title();
    }

    public SelenideElement getReversedString()
    {
        return $(byId("result"));
    }

    public void enterString(String stringToReverse)
    {
        $(byId("normalstring")).val(stringToReverse);
        $(byId("submitbutton")).click();
    }

    public boolean isPalindrome()
    {
        return !$(byId("palindrome")).has(Condition.cssClass("hidepalindrome"));
    }
}
