package bindings;

import com.codeborne.selenide.Condition;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import pageobjects.StringReverserPage;

import java.net.MalformedURLException;
import java.net.URL;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.codeborne.selenide.WebDriverRunner.setWebDriver;
import static org.assertj.core.api.Assertions.assertThat;

public class Stringreverser {

    @Before
    public void setRemoteWebDrivers() throws MalformedURLException {
        String urlToRemoteWD = "http://localhost:4444/wd/hub";
        RemoteWebDriver driver =new RemoteWebDriver(new URL(urlToRemoteWD), DesiredCapabilities.chrome());
        setWebDriver(driver);
    }

    @Given("the user is on the stringreverser page")
    public void the_user_is_on_the_stringreverser_page() {
        StringReverserPage stringReverserPage = new StringReverserPage();
        open("http://StringReverserWebSite");
        stringReverserPage.getPageTitle().contains("String Reverser");
    }

    @When("the user enters the string {string}")
    public void the_user_enters_the_string(String string) {
        StringReverserPage stringReverserPage = new StringReverserPage();
        stringReverserPage.enterString(string);
    }

    @Then("the reversed string should be {string}")
    public void the_reversed_string_should_be(String string) {
        StringReverserPage stringReverserPage = new StringReverserPage();
        stringReverserPage.getReversedString().shouldHave(Condition.text(string));
    }

    @Then("be identified as a palindrome")
    public void be_identified_as_a_palindrome() {
        StringReverserPage stringReverserPage = new StringReverserPage();
        assertThat(stringReverserPage.isPalindrome()).isTrue();
    }

    @After
    public void quitWebDriver()
    {
        getWebDriver().quit();
    }
}
